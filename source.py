'''
Source code used in the analysis conducted in "Machine Learning Models and
Features Selection for Modelling Apache Spark Performance", Francesco Clerici,
2018.
'''

import csv
import numpy as np
import sklearn.svm
import pandas
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestRegressor
from statsmodels.stats.outliers_influence import variance_inflation_factor
from pandas import DataFrame
from numpy import random

######################## PREPROCESSING ########################

print()
print('1) Preprocessing...')
print()
with open('query40_reduced.csv', 'r') as f:
    reader = csv.reader(f)
    dataset = np.array(list(reader))

# Remove contour
dataset = dataset[1:,1:]
labels = dataset[0,2:]
appLabels = dataset[1:,0]
dataset = dataset[1:,1:]
dataset = dataset.astype(np.float)

# Transform: nContainers -> 1/nContainers
dataset[:,dataset.shape[1]-1] = 1/dataset[:,dataset.shape[1]-1]
labels[dataset.shape[1]-2] = '1/nContainer'
maxT = np.max(dataset[:,0])
minT = np.min(dataset[:,0])

# Scale into [0,1]
scaler = MinMaxScaler()
dataset = scaler.fit_transform(dataset)

# Remove constant columns
table = np.all(dataset == dataset[0,:], axis = 0)
table = np.array(list(map (lambda x: not x,table)))
dataset_nonConst = dataset[:,table]
labels_nC = labels[table[1:]]

# Divide into response and variables
yy = dataset[:,0]
XX = dataset_nonConst[:,1:]

# Drop the collinear columns
def calculateVIF(X, tt=5.0):
    X = pandas.DataFrame(data=X)
    variables = list(range(X.shape[1]))
    dropped=True
    while dropped:
        dropped=False
        vif = [variance_inflation_factor(X[variables].values, ix) for ix\
              in range(X[variables].shape[1])]
        maxloc = vif.index(max(vif))
        if max(vif) > tt:
            del variables[maxloc]
            dropped=True
    return DataFrame.as_matrix(X[variables]), np.array(variables)

X_new, vv = calculateVIF(XX,10.0)
nCont = XX[:,XX.shape[1]-1]
dataSize = XX[:,XX.shape[1]-2]
labels_new = labels_nC[vv]

# Include dataSize and 1/nContainers if not included
if 'dataSize' not in labels_new:
    X_new = np.c_[X_new,dataSize]
    labels_new = np.append(labels_new,'dataSize')
    vv = np.append(vv,XX.shape[1]-2)
if '1/nContainer' not in labels_new:
    X_new = np.c_[X_new,nCont]
    labels_new = np.append(labels_new,'1/nContainers')
    vv = np.append(vv,XX.shape[1]-1)

# Out-of-bootstrap validation preparation
D = X_new.shape[1]
N = X_new.shape[0]
KK = 30
NN = int(N/KK)
train_ind = np.zeros((KK,N))
test_ind = np.zeros((KK,NN))

for ii in range(0,KK):
    test_ind[ii] = random.choice(np.arange(0,N),NN,replace=False)
    diff = np.setdiff1d(np.arange(0,N),test_ind[ii])
    enrich_arr = random.choice(diff,NN)
    train_ind[ii] = np.concatenate((diff,enrich_arr))

train_ind = train_ind.astype(np.int)
test_ind = test_ind.astype(np.int)

######################## COMPARISON: SVR vs RF ########################
print('2) Comparison between SVR and RF...')
print()
# Useful function
def errorABS(yhh,ytt,M,m):
    yh = yhh*(M-m)+m
    yt = ytt*(M-m)+m
    return 100*np.mean(np.absolute(yt-yh)/np.absolute(yt))

# SVR params (based on the whole dataset
# for simplicity)
C_in = max(abs(np.mean(yy)-3*np.std(yy)),abs(np.mean(yy)+3*np.std(yy)))
epsilon_in = 3*np.std(yy)*(np.log(N)/N)**(.5)

# sklearn estimators
suver = sklearn.svm.SVR(C=C_in, epsilon=epsilon_in)
randf = RandomForestRegressor(n_estimators=10,max_depth=100,max_features=10)

# Errors allocation
err_rf = np.zeros(KK)
err_svr = np.zeros(KK)

# Compute errors
for ii in range(0,KK):
    X_train = XX[train_ind[ii]]
    y_train = yy[train_ind[ii]]
    X_test = XX[test_ind[ii]]
    y_test = yy[test_ind[ii]]
    randf.fit(X_train,y_train)
    suver.fit(X_train,y_train)
    y_hat_rf = randf.predict(X_test)
    y_hat_svr = suver.predict(X_test)
    err_rf[ii] = errorABS(y_hat_rf,y_test,maxT,minT)
    err_svr[ii] = errorABS(y_hat_svr,y_test,maxT,minT)

# Plot
xx = np.arange(KK)+1
ax = plt.subplot(111)
ax.bar(xx-0.15,err_svr,label='svr',color='blue',\
       align='center',width=0.3)
ax.bar(xx+0.15,err_rf,label='random forest',color='red',\
       align='center',width=0.3)
ax.legend()
ax.grid()
plt.title('Normalized MAE from SVR and Random Forest')
plt.ylabel('MAE [%]')
plt.xlabel('fold')
plt.show()

######################## FEATURE SELECTION ########################
print('3) Feature selection...')
print()
# Fit and allocate
randf.fit(X_new,yy)
ranking_features = np.ones((labels_new.shape[0]))

# Features ordered by impurity
aux = sorted(zip(randf.feature_importances_,labels_new),reverse=True)
for ii in range(0,labels_new.shape[0]):
    ranking_features[ii] = int(list(labels_new).index(aux[ii][1]))

ranking_features = ranking_features.astype(int)

# Add one after another the most important features
scores = np.zeros((ranking_features.shape[0]-1,KK))
for jj in range(1,ranking_features.shape[0]):
    m_feat = min(10,jj)
    X_reduced = X_new[:,ranking_features[0:jj]]
    for kk in range(0,KK):
        randf = RandomForestRegressor(n_estimators=10,\
                    max_depth=100,max_features=m_feat)
        randf.fit(X_reduced[train_ind[kk],:],yy[train_ind[kk]])
        y_hat = randf.predict(X_reduced[test_ind[kk],:])
        scores[jj-1,kk] = mean_squared_error(yy[test_ind[kk]],y_hat)

# Average the scores w.r.t. the folds
scores = np.mean(scores,axis=1)
plt.figure()
plt.plot(np.arange(0,ranking_features.shape[0]-1),scores,linewidth=3)
plt.grid()
plt.title('MSE of the random forest increasing the number of features')
plt.ylabel('MSE')
plt.xlabel('#feats considered')
plt.savefig('errDecreasing.png')
plt.show()
print('Ranking of the features:')
print(labels_new[ranking_features])
print()

######################## BILINEAR TERM SELECTION ########################
print('4) Bilinear terms selection... (takes a while)')
print()
# Allocation and useful function
scores = np.zeros((KK,int(D*D - D*(D-1)/2),3))
def addBilinearTerm(X,i,j):
    new_col = np.multiply(X[:,i],X[:,j])
    return np.c_[X, new_col]

# Long computation
for kk in range(0,KK):
    ss = 0
    for ii in range(0,D):
        for jj in range(ii,D):
            X_with_bil = addBilinearTerm(X_new,ii,jj)
            XX_train_new = X_with_bil[train_ind[kk]]
            XX_test_new = X_with_bil[test_ind[kk]]
            randf.fit(XX_train_new,yy[train_ind[kk]])
            y_hat = randf.predict(XX_test_new)
            scores[kk,ss] = [mean_squared_error(yy[test_ind[kk]],y_hat),ii,jj]
            ss += 1

# Order the scores
for ii in range(0,KK):
    aux = scores[ii]
    scores[ii,:,:] = aux[aux[:,0].argsort()]

# Plot the result for the first folds
plt.figure()
plt.plot(scores[0,:,0],linewidth=3)
plt.grid()
plt.title('MSE (increasing order) of all the possible bilinear terms')
plt.ylabel('MSE')
plt.xlabel('Bilinear term index')
plt.savefig('errBilTerms.png')
plt.show()

indices_and_votes = np.zeros((int(D*D - D*(D-1)/2),3))
ss = 0
for ii in range(0,D):
    for jj in range(ii,D):
        indices_and_votes[ss] = [ii,jj,0]
        ss += 1

for kk in range(0,KK):
    for ii in range(0,20):
        for jj in range(0,int(D*D - D*(D-1)/2)):
            if indices_and_votes[jj,0] == scores[kk,ii,1] and\
                indices_and_votes[jj,1] == scores[kk,ii,2]:
                indices_and_votes[jj,2] += 1

# Order the votes
indices_and_votes = indices_and_votes[\
            (-indices_and_votes[:,2]).argsort()]

print('First 10 most voted couples:')
for ii in range(0,10):
    print(ii+1,') ',labels_new[int(indices_and_votes[ii,0])],\
          '*',labels_new[int(indices_and_votes[ii,1])],\
          '--> #votes = ',indices_and_votes[ii,2])
print()

######################## EXTRAPOLATION TEST ########################
print('5) Extrapolation test...')
print()
# Split dataset
# A = 250
# B = 750
# C = 1000
XA = XX[XX[:,XX.shape[1]-2]==0]
yA = yy[XX[:,XX.shape[1]-2]==0]
XB = XX[(XX[:,XX.shape[1]-2]!=0) & (XX[:,XX.shape[1]-2]!=1)]
yB = yy[(XX[:,XX.shape[1]-2]!=0) & (XX[:,XX.shape[1]-2]!=1)]
XC = XX[XX[:,XX.shape[1]-2]==1]
yC = yy[XX[:,XX.shape[1]-2]==1]
XAB = XX[XX[:,XX.shape[1]-2]!=1]
yAB = yy[XX[:,XX.shape[1]-2]!=1]
XAC = XX[(XX[:,XX.shape[1]-2]==0) | (XX[:,XX.shape[1]-2]==1)]
yAC = yy[(XX[:,XX.shape[1]-2]==0) | (XX[:,XX.shape[1]-2]==1)]
XBC = XX[XX[:,XX.shape[1]-2]!=0]
yBC = yy[XX[:,XX.shape[1]-2]!=0]

# Select uncorrelated features
XA = XA[:,vv]
XB = XB[:,vv]
XC = XC[:,vv]
XAB = XAB[:,vv]
XBC = XBC[:,vv]
XAC = XAC[:,vv]

# Add bilinear terms
bb = np.zeros((5,2))
for kk in range(0,5):
    bb[kk,0] = int(indices_and_votes[kk,0])
    bb[kk,1] = int(indices_and_votes[kk,1])

bb = bb.astype(np.int)

for ii in range(0,5):
    XA = addBilinearTerm(XA,bb[ii,0],bb[ii,1])
    XB = addBilinearTerm(XB,bb[ii,0],bb[ii,1])
    XC = addBilinearTerm(XC,bb[ii,0],bb[ii,1])
    XAB = addBilinearTerm(XAB,bb[ii,0],bb[ii,1])
    XBC = addBilinearTerm(XBC,bb[ii,0],bb[ii,1])
    XAC = addBilinearTerm(XAC,bb[ii,0],bb[ii,1])

# Collect
data = []
data.append(XA)
data.append(XB)
data.append(XC)
data.append(XAB)
data.append(XAC)
data.append(XBC)
resp = []
resp.append(yA)
resp.append(yB)
resp.append(yC)
resp.append(yAB)
resp.append(yAC)
resp.append(yBC)

# Allocation
scores = np.zeros((6,6))
aux = np.zeros((6,6))

# Fit
for ii in range(0,6):
    randf.fit(data[ii],resp[ii])
    for jj in range(0,6):
        y_hat = randf.predict(data[jj])
        scores[ii,jj] = errorABS(y_hat,resp[jj],maxT,minT)
        aux[ii,jj] = round(scores[ii,jj],4)


# Print the results
print('First row = training set')
print('First col = test set')
teams_list = ["250", "750", "1000","250+750","250+1000","750+1000"]
row_format ="{:>15}" * (len(teams_list) + 1)
print(row_format.format("", *teams_list))
for team, row in zip(teams_list, aux):
    print(row_format.format(team, *row))

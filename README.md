# README #

### Brief description ###

* Source code used in the analysis conducted in "Machine Learning Models and Features Selection for Modelling Apache Spark Performance", Francesco Clerici, 2018.
* The code is divided into 5 parts: Preprocessing, SVR-RF comparison, feature selection, bilinear terms selection and extrapolation test.
* A reduced version of dataset query Q40 is available.
* It is recommended to run with 'python -W ignore' options.
